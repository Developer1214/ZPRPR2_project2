#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define ID 12
#define SUBJECT 152
#define NAME 302
#define DATE 14
#define MAXSIZE 302
#define ATR 4

typedef struct{
    char * name;
    char * lastname;
    char * role;
} Persons;

typedef struct item{
    char * id;
    Persons * persons;
    int persons_count;
    char * subject;
    char * date;

    struct item *next;
} Item;



void nameFinder(Item ** item,char * name){
    char * tmp_p = name;
    while(1){
        char * name = strtok(tmp_p," ");
        if (name == NULL) {
            break;
        }
        name[strlen(name)] = '\0';
    
    
        char * lastname = strtok(NULL, "#");
        if (lastname == NULL) {
            break;
        }
        lastname[strlen(lastname)] = '\0';
    
    
        char * role = strtok(NULL, "#");
        if (role == NULL) {
            break;
        }
        role[strlen(role)] = '\0';
    
        
        int num = ++(*item)->persons_count;
        
        (*item)->persons = (Persons *)realloc((*item)->persons, sizeof(Persons) * num);
        (*item)->persons[num - 1].name = (char *)malloc(sizeof(char)*strlen(name) + 1);  
        (*item)->persons[num - 1].lastname = (char *)malloc(sizeof(char)*strlen(lastname) + 1);
        (*item)->persons[num - 1].role = (char *)malloc(sizeof(char)*strlen(role) + 1);
    
        strcpy((*item)->persons[num - 1].name,name); 
        strcpy((*item)->persons[num - 1].lastname,lastname);
        strcpy((*item)->persons[num - 1].role,role);
        tmp_p = NULL;
    }
    tmp_p = NULL;
}


void miniDealloc(Item **item) {
    Item * present_item = *item;
    while(present_item != NULL) {
        Item * nextItem = present_item->next;
        free(present_item->id);
        present_item->id = NULL;
        free(present_item->subject);
        present_item->subject = NULL;
        for (int j = 0; j<present_item->persons_count; j++) {
            free(present_item->persons[j].name);
            present_item->persons[j].name = NULL;
            free(present_item->persons[j].lastname);
            present_item->persons[j].lastname = NULL;
            free(present_item->persons[j].role);
            present_item->persons[j].role = NULL;
        }
        free(present_item->persons);
        present_item->persons = NULL;
        free(present_item->date);
        free(present_item);     
        present_item = nextItem;
    }
    *item = NULL;

}


void dealloc(FILE ** f,Item ** item){
    if(item != NULL){miniDealloc(item);}
    if(*f != NULL){fclose(*f);*f = NULL;}
    exit(0);
}

void clear_stdin() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

void analize_date_p(char * date){
    char day[3], month[3], year[5], hour[3], minute[3];
    sscanf(date,"%4s%2s%2s%2s%2s\n",year,month,day,hour,minute); 
    printf("Datum a Cas: %s.%s.%s %s:%s\n",year,month,day,hour,minute);
}


void read(FILE **f,Item ** startItem){
    if(*f == NULL){
        *f = fopen("KonferencnyZoznam2.txt", "r");
        if(*f == NULL){
            printf("Zaznamy neboli nacitane!\n");
            return;
        }
    }
    //Dealloc
    if(*startItem != NULL){
        miniDealloc(startItem);
        rewind(*f);
    }
    int counter = 0;
    char * pole = malloc(sizeof(char)* 5);
    while(!feof(*f) && fgets(pole,5,*f) != NULL){
        if(strcmp(pole,"$$$\n") == 0){
            counter++;
        }
        Item * newItem = (Item *)malloc(sizeof(Item));
        //Get id
        newItem->id = (char *)malloc(sizeof(char)*ID);  
        fgets(newItem->id, ID, *f);
        //Get sub
        newItem->subject = (char *)malloc(sizeof(char)*SUBJECT);
        fgets(newItem->subject, SUBJECT, *f);
        newItem->subject[strcspn(newItem->subject, "\n")] = '\0';
        //Get name
        newItem->persons_count = 0;
        newItem->persons = NULL;
        
        char * name = (char *)malloc(sizeof(char)*NAME);
        fgets(name, NAME, *f);
        nameFinder(&newItem, name);
        free(name);
        name = NULL;

        //Get date
        newItem->date = (char *)malloc(sizeof(char)*DATE);
        fgets(newItem->date, DATE, *f);
        
        //Initialze next pointer
        newItem->next = NULL;

        //Check if it's first item
        if(*startItem == NULL){
            *startItem = newItem;
            continue;
        }
        //Find last item and give him next item -> newItem
        Item * last = *startItem;
        while(last->next != NULL){
            last = last->next;
        }

        last->next = newItem;
    }
    free(pole);
    pole = NULL;

    printf("Nacitalo sa %d zaznamov\n",counter); 
}

void print(Item ** item){
    if(*item == NULL){
        printf("Prázdny zoznam záznamov.\n");
        return;
    }
    Item * present_item = *item;
    for(int i = 1;present_item != NULL;i++){
        printf("\033[1m%d.\033[0m\n",i);
        printf("ID prispevku : %s",present_item->id);   
        printf("Nazov prispevku: %s\n", present_item->subject);
        printf("Mena autorov:\n");
        for(int j = 0;j<present_item->persons_count;j++){
            printf("\t%d: %s %s (%s)\n", 
                    j+1,
                    present_item->persons[j].name,
                    present_item->persons[j].lastname,
                    present_item->persons[j].role);      

        }
        analize_date_p(present_item->date);
        present_item = present_item->next;
    }
}
            
void add(Item **startItem) {
    Item *newItem = (Item *)malloc(sizeof(Item));
    newItem->id = (char *)malloc(sizeof(char) * ID);
    newItem->subject = (char *)malloc(sizeof(char) * SUBJECT);
    newItem->persons = NULL;
    newItem->persons_count = 0;
    newItem->date = (char *)malloc(sizeof(char) * DATE);
    newItem->next = NULL;

    int num = 0;
    fscanf(stdin, "%d\n", &num);
    fflush(stdin);
    fgets(newItem->id, ID, stdin);
    fflush(stdin);
    fgets(newItem->subject, SUBJECT, stdin);
    newItem->subject[strcspn(newItem->subject, "\n")] = '\0';
    fflush(stdin);
    char *name = malloc(sizeof(char) * NAME);
    fgets(name, NAME, stdin);
    fflush(stdin);
    nameFinder(&newItem, name);
    free(name);
    name = NULL;
    fgets(newItem->date, DATE, stdin);

    if (*startItem == NULL) {
        *startItem = newItem;
        return;
    }

    Item *present = *startItem;
    Item *previous = NULL;

    for (int i = 0; i < num - 1 && present != NULL; i++) {
        previous = present;
        present = present->next;
    }

    if (previous == NULL) {
        newItem->next = *startItem;
        *startItem = newItem;
    } else {
        previous->next = newItem;
        newItem->next = present;
    }
}

void delete(Item **startItem) {
    if (*startItem == NULL) {
        printf("Prázdny zoznam záznamov.\n");
        return;
    }
    Item *presentItem = *startItem;
    Item *previousItem = NULL;

    char *name = malloc(sizeof(char) * NAME);
    fgets(name, NAME, stdin);

    char * firstname = strtok(name, " ");
    firstname[strlen(firstname)] = '\0';
    char * lastname = strtok(NULL, "#");
    lastname[strlen(lastname)-1] = '\0';
    while (presentItem != NULL) {
        bool isAutor = false;
        for(int i = 0;i<presentItem->persons_count;i++){
            if(strcmp(presentItem->persons[i].name, firstname) == 0 &&
                strcmp(presentItem->persons[i].lastname, lastname) == 0 &&
                strcmp(presentItem->persons[i].role, "A") == 0){
                isAutor = true;
            }
        }
        if (isAutor){
            free(presentItem->id);
            presentItem->id = NULL;

            //Save name of subject for printf
            char * subject = malloc(sizeof(char)*strlen(presentItem->subject) + 1);
            strcpy(subject,presentItem->subject);
            free(presentItem->subject);
            presentItem->subject = NULL;
            for (int i = 0; i < presentItem->persons_count; i++) {
                free(presentItem->persons[i].name);
                free(presentItem->persons[i].lastname);
                free(presentItem->persons[i].role);
            }
            free(presentItem->persons);
            free(presentItem->date);
            //If not the first item
            if (previousItem != NULL) {
                previousItem->next = presentItem->next;
            } else {
                *startItem = presentItem->next;
            }

            Item *nextItem = presentItem->next;
            free(presentItem);
            presentItem = nextItem;
            printf("Prispevok s názvom %s bol vymazany.\n",subject);
            //Dealloc subject
            free(subject);
            subject = NULL;
        } else {
            previousItem = presentItem;
            presentItem = presentItem->next;
        }
    }
    free(name);
    name = NULL;
}

void update(Item ** startItem){
    if (*startItem == NULL) {
        printf("Prázdny zoznam záznamov.\n");
        return;
    }

    Item * presentItem = *startItem;
    
        
    char * id = malloc(sizeof(char)*9);
    char * type = malloc(sizeof(char)*3);
    char * buff = malloc(sizeof(char)*12);
    
    while(1){

        fgets(buff,12,stdin);
        sscanf(buff, "%8s %2s",id,type);
        if(atoi(id) % 15 != 0 ||
            ( 
            strcmp(type,"PD") != 0 &&
            strcmp(type, "UD") != 0 &&
            strcmp(type, "PP") != 0 &&
            strcmp(type, "UP") != 0
            )
        ){
            printf("Zadane udaje nie su korektne, zadaj novy retazec: ");
            clear_stdin();
        }else{
            break;
        }
    }
    while(presentItem != NULL){
        char * itemId = malloc(sizeof(char)*9);
        sscanf(presentItem->id, "%*2s%8s",itemId);
        if(strcmp(itemId,id) == 0){
            printf("Prispevok s nazvom %s sa bude prezentovat %s [%c%c]\n",presentItem->subject,type,presentItem->id[0],presentItem->id[1]);    
            presentItem->id[0] = type[0];
            presentItem->id[1] = type[1];
        }
        presentItem = presentItem->next;
        free(itemId);
        itemId = NULL;
    }
    clear_stdin();
    free(buff);
    buff = NULL;
    free(id);
    id = NULL;
    free(type);
    type = NULL;
}

void swap(Item ** startItem){
    if (*startItem == NULL) {
        printf("Prázdny zoznam záznamov.\n");
        return;
    }
    int c1,c2;
    if(fscanf(stdin, "%d %d\n",&c1,&c2) != 2) return;
    
    if (c1 == c2) {
        return; 
    }
    Item * presentItem = *startItem;
    Item * item1 = NULL;
    Item * item2 = NULL;
    Item * prev1 = NULL;
    Item * prev2 = NULL;
    
    int index = 1;

    while (presentItem != NULL) {
        if (c1 == 1 && index == 1) {
            item1 = presentItem;
        } else if (c2 == 1 && index == 1) {
            item2 = presentItem;
        }
        if(index == c1 - 1) {
            prev1 = presentItem;
            item1 = presentItem->next;
        }else if (index == c2 - 1) {
            prev2 = presentItem;
            item2 = presentItem->next;
        }
        presentItem = presentItem->next;
        index++;
    }
    if(item1 == NULL || item2==NULL){return;}
    if(prev1 != NULL){
        prev1->next = item2;
    }else {
        *startItem = item2;
    }

    if(prev2 != NULL){
        prev2->next = item1;
    }else{
        *startItem = item1;
    }
    Item * tmp = item1->next;
    item1->next = item2->next;
    item2->next = tmp;

       

}

int main(){
    char command;
    FILE *f = NULL;
    Item * startItem = NULL;
    while(1){
        scanf("%1c",&command);
        clear_stdin();
        fflush(stdin);
        switch (command) {
            case 'n': read(&f,&startItem);break;
            case 'v': print(&startItem);break;
            case 'p': add(&startItem);break;
            case 'z': delete(&startItem);break;
            case 'k': dealloc(&f,&startItem);break;
            case 'r': swap(&startItem);break;
            case 'a': update(&startItem);break;
            default: printf("Neznamy prikaz\n");break;
        }
    }
    return 0;
}
